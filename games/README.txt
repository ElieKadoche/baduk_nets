=====================================
ELF OpenGo Selfplay and Model Dataset
=====================================

We have released all the generated selfplays and intermediate models from the training of our final model. The URLs of these resources may be found in the CSV file located at https://dl.fbaipublicfiles.com/elfopengo/v2_training_run/urls.csv .

We provide a "full" selfplay dataset containing a total of roughly 20 million selfplays, and a "mini" selfplay dataset containing 500 randomly sampled selfplays per model version (for a total of 0.75 million selfplays).

The columns of the CSV are as follows:

- model_version:
      numeric, between 0 and 1500000
- model_url:
      URL of the binary file containing the model parameters
- selfplay_json_url:
      URL of the full xz-compressed selfplay JSON records generated using the model (most detailed)
- selfplay_sgf_url:
      URL of the full xz-compressed selfplay SGF tarballs generated using the model
- mini_selfplay_json_url:
      URL of the mini-dataset xz-compressed selfplay JSON records generated using the model
- mini_selfplay_sgf_url:
      URL of the mini-dataset xz-compressed selfplay SGF tarballs generated using the model
- num_games:
      numeric: number of selfplay games in selfplay_json_url and selfplay_sgf_url

Rate limiting
-------------

To ensure that your IP does not get banned, please follow the rate limiting instructions provided at https://dl.fbaipublicfiles.com/README (see below).

-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------

This file server contains datasets, pretrained models, and other resources
released by Facebook AI Research.

Rate limiting
-------------

To prevent abuse, we rate limit downloads from this file server. If you plan to
download more than 100 files, then please limit your downloads to one file
every three seconds. Failure to comply may result in a temporary or permanent
IP ban.
