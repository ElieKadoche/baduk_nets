import argparse

import matplotlib.pyplot as plt
import pandas as pd

import src

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Scipt to create graphs \
            from training log. 2 files will be created, one for \
            the policy head and one for the value head")
    parser.add_argument(
        "--path",
        help="The name of the training log file",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--output",
        help="Begin path of the 2 output files. Default is \
            {}training_results_".format(src.RESULTS_DIR),
        type=str,
        default="{}training_results_".format(src.RESULTS_DIR),
    )

    args = parser.parse_args()
    src.create_dir_if_not_found(src.RESULTS_DIR)

    # Reading data
    # -------------------------

    print("Reading data...", end="")

    data = pd.read_csv(args.path, sep=";")

    policy_loss = data["policy_loss"]
    value_loss = data["value_loss"]
    policy_accuracy = data["policy_accuracy"]
    value_accuracy = data["value_accuracy"]

    # If valid
    # val_policy_loss = data["val_policy_loss"]
    # val_value_loss = data["val_value_loss"]
    # val_policy_accuracy = data["val_policy_accuracy"]
    # val_value_accuracy = data["val_value_accuracy"]

    print(" Done!")

    # Saving graphs
    # -------------------------

    print("Saving graphs...", end="")

    # Policy head
    fig, ax = plt.subplots(
        nrows=1, ncols=2, num="Policy head", figsize=(11, 4))
    ax[0].plot(policy_loss, color="b")
    # ax[0].plot(val_policy_loss, color="r")  # If valid
    ax[0].title.set_text("Policy loss (categorical crossentropy)")
    ax[0].set_xlabel("Epoch")
    ax[0].set_ylabel("Loss")
    ax[0].legend(["Train"], loc="upper right")
    # ax[0].legend(["Train", "Valid"], loc="upper right")  # If valid

    ax[1].plot(policy_accuracy, color="c")
    # ax[1].plot(val_policy_accuracy, color="r")  # If valid
    ax[1].title.set_text("Policy accuracy")
    ax[1].set_xlabel("Epoch")
    ax[1].set_ylabel("Accuracy")
    # ax[1].legend(["Train", "Valid"], loc="upper right")

    fig.savefig("{}policy.png".format(args.output), dpi=400)

    # Value head
    fig, ax = plt.subplots(nrows=1, ncols=2, num="Value head", figsize=(11, 4))
    ax[0].plot(value_loss, color="b")
    # ax[0].plot(val_value_loss, color="r")  # If valid
    ax[0].title.set_text("Value loss (mean squared error)")
    ax[0].set_xlabel("Epoch")
    ax[0].set_ylabel("Loss")
    ax[0].legend(["Train"], loc="upper right")
    # ax[0].legend(["Train", "Valid"], loc="upper right")  # If valid

    ax[1].plot(value_accuracy, color="c")
    # ax[1].plot(val_value_accuracy, color="r")  # If valid
    ax[1].title.set_text("Value accuracy")
    ax[1].set_xlabel("Epoch")
    ax[1].set_ylabel("Accuracy")
    # ax[1].legend(["Train", "Valid"], loc="upper right")

    fig.savefig("{}value.png".format(args.output), dpi=400)

    print(" Done!")
