import argparse
import os
import random
import shutil
import tarfile
import urllib.request

import pandas as pd
import tqdm

import src

train_dir = "{}sgf_elf_train".format(src.GAMES_DIR)
test_dir = "{}sgf_elf_test".format(src.GAMES_DIR)

if __name__ == "__main__":
    # Parser
    # -------------------------

    parser = argparse.ArgumentParser(
        description="Script to download ELF selfplay games. Games will be \
        saved in {}, except <nb_test_games> of the last model version \
        which will be saved in {}".format(train_dir, test_dir)
    )
    parser.add_argument(
        "--min-model",
        help="Minimum model version. Default is 1466000",
        default=1466000,
        type=int,
    )
    parser.add_argument(
        "--max-model",
        help="Maximum model version. Default is 1499000",
        default=1499000,
        type=int,
    )
    parser.add_argument(
        "--nb-test-games",
        help="Number of games of the last model version to keep for testing. \
            Default is 503",
        default=503,
        type=int,
    )

    args = parser.parse_args()

    # Clear folders
    # -------------------------

    shutil.rmtree(train_dir, ignore_errors=True)
    shutil.rmtree(test_dir, ignore_errors=True)

    os.makedirs(train_dir)
    os.makedirs(test_dir)

    # Get URLS
    # -------------------------

    data = pd.read_csv(
        "https://dl.fbaipublicfiles.com/elfopengo/v2_training_run/urls.csv")

    # Games from models min_model to max_model - 1000
    # -------------------------

    data1 = data[(data.model_version >= args.min_model) & (
        data.model_version <= args.max_model - 1000)]

    for sgf_url in tqdm.tqdm(
        data1.selfplay_sgf_url, desc="Creating SGF (1)", total=data1.shape[0]
    ):
        tar_name = sgf_url.split("/")[-1]

        # If we dowload more than 100 files, we need to wait 3 seconds
        # between each file (FaceBook rule)
        urllib.request.urlretrieve(sgf_url, tar_name)

        with tarfile.open(tar_name) as tar:
            tar.extractall("{}/".format(train_dir))

        os.remove(tar_name)

    # Games from last model, keep <nb_test_games> for test
    # -------------------------

    print("Creating SGF (2), model version {}, keeping {} games for \
            testing...".format(args.max_model, args.nb_test_games), end="",)
    data2 = data[data.model_version == args.max_model]

    index = 0
    random.seed(42)
    nb_games = data2.num_games.iloc[0]
    test_indexes = random.sample(range(0, nb_games), args.nb_test_games)

    sgf_url = data2.selfplay_sgf_url.iloc[0]
    tar_name = sgf_url.split("/")[-1]
    urllib.request.urlretrieve(sgf_url, tar_name)

    with tarfile.open(tar_name) as tar:
        for m in tar.getmembers():

            if index in test_indexes:
                tar.extract(m, "{}/".format(test_dir))

            elif index not in test_indexes:
                tar.extract(m, "{}/".format(train_dir))

            index += 1

    os.remove(tar_name)
    print(" Done!")
