# baduk_nets

This projects aims to create artificial neural networks for the game of Go, trained with supervised learning.

## Presentation

This is the project of the Deep Learning course of the [IASD Master](https://www.lamsade.dauphine.fr/wp/iasd/en/) (Artificial Intelligence Systems and Data Science). More information can be found [here](https://www.lamsade.dauphine.fr/~cazenave/DeepLearningProject.html).

## Restriction

The maximum number of parameters allowed for the model in 1000000. The last model has 986026 parameters.

## Getting Started

### Prerequisites

You need to have Python 3.7 at least and CMake installed on your system.

### Installing

- Clone the repository and install required packages.

```shell
git clone https://gitlab.com/ElieKadoche/klanoceptik_baduk_ai.git
git submodule init
git submodule update
pip3 install -r requirements.txt
```

- Install the `golois` package. This packages allows to use dynamic batches.

```shell
pip install ./golois
```

### Get data

- Data comes from selfplay games of the FaceBook engine [ELF](https://ai.facebook.com/tools/elf). To download it, use the following command. For more information, use `-h` and check this [`README`](games/README.txt). The games will be saved in `.sgf` format in the [`games`](games) folder.

```shell
python download_elf_games.py --min-model 1466000 --max-model 1499000 --nb-test-games 503
```

- Create `games.txt` and `games.data` files for testing and training. For more information, use `-h`. You have 2 choices.

  - If you want to use a training and a testing dataset, execute the following command. The second command will also save data in `.npy` files in `games/data`.

  ```shell
  python create_games_data.py train
  python create_games_data.py test
  ```

  - If do not want to use a testing dataset and therefore use all data for training, execute the following command.

  ```shell
  python create_games_data.py all
  ```

## Usage

There is one script to train and test the network. To get info, you can type `python train_neural_net.py -h`.

```
usage: train_neural_net.py [-h] [-w WEIGHTS] [--output-log OUTPUT_LOG]
                           [--output-info OUTPUT_INFO] [--gpu GPU]
                           [--dynamic-batch] [--no-test-data]
                           [--data-augmentation] [--data-selection]
                           [-e EPOCHS] [-b BATCH_SIZE] [-v]
                           [--tf-log-level {0,1,2,3}]
                           {train,info,all}

Script to deal with a neural network (ResNet model) for the game of Go.

positional arguments:
  {train,info,all}      You can either train the network, get info or both.
                        Default is all

optional arguments:
  -h, --help            show this help message and exit
  -w WEIGHTS, --weights WEIGHTS
                        h5 file from which load (if it exists) and save the
                        model weights. Default is models/neural_net.h5
  --output-log OUTPUT_LOG
                        Output file name of training info. Default is
                        results/neural_net.csv
  --output-info OUTPUT_INFO
                        Output file name of model info. Default is
                        results/neural_net_info.txt
  --gpu GPU             The ID of the GPU (ordered by PCI_BUS_ID) to use. If
                        not set, no GPU configuration is done. Default is None
  --dynamic-batch       If set, new training data will be generated after each
                        epoch
  --no-test-data        Is set, training will be done on all data (no testing)
  --data-augmentation   If set, data augmentation will be done on batches
  --data-selection      If set, data selection will be done each time new
                        samples are generated using dynamic batch
  -e EPOCHS, --epochs EPOCHS
                        Number of epochs. Default is 20
  -b BATCH_SIZE, --batch-size BATCH_SIZE
                        Batch size. Default is 256
  -v, --verbose         If set, output details of the execution
  --tf-log-level {0,1,2,3}
                        Tensorflow minimum cpp log level. Default is 0
```
