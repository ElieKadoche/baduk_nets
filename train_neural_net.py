import argparse
import os

import numpy as np
import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.callbacks import CSVLogger, ModelCheckpoint

import src

tf.keras.backend.clear_session()

# Parser configuration
# -------------------------

parser = argparse.ArgumentParser(description="Script to deal with a neural \
        network (ResNet model) for the game of Go.")
parser.add_argument(
    "mode",
    help="You can either train the network, get info or both. Default is all",
    choices=["train", "info", "all"],
    type=str,
    default="all",
)
parser.add_argument(
    "-w",
    "--weights",
    help="h5 file from which load (if it exists) and save the model weights. \
        Default is {}neural_net.h5".format(src.MODELS_DIR),
    type=str,
    default="{}neural_net.h5".format(src.MODELS_DIR),
)
parser.add_argument(
    "--output-log",
    help="Output file name of training info. Default is \
        {}neural_net.csv".format(src.RESULTS_DIR),
    type=str,
    default="{}neural_net.csv".format(src.RESULTS_DIR),
)
parser.add_argument(
    "--output-info",
    help="Output file name of model info. Default is \
        {}neural_net_info.txt".format(src.RESULTS_DIR),
    type=str,
    default="{}neural_net_info.txt".format(src.RESULTS_DIR),
)
parser.add_argument(
    "--gpu",
    help="The ID of the GPU (ordered by PCI_BUS_ID) to use. \
        If not set, no GPU configuration is done. Default is None",
    type=int,
    default=None,
)
parser.add_argument(
    "--dynamic-batch",
    help="If set, new training data will be generated after each epoch",
    action="store_true",
)
parser.add_argument(
    "--no-test-data",
    help="Is set, training will be done on all data (no testing)",
    action="store_true",
)
parser.add_argument(
    "--data-augmentation",
    help="If set, data augmentation will be done on batches",
    action="store_true",
)
parser.add_argument(
    "--data-selection",
    help="If set, data selection will be done each time new samples \
        are generated using dynamic batch",
    action="store_true",
)
parser.add_argument(
    "-e",
    "--epochs",
    help="Number of epochs. Default is 20",
    type=int,
    default=20,
)
parser.add_argument(
    "-b",
    "--batch-size",
    help="Batch size. Default is 256",
    type=int,
    default=256,
)
parser.add_argument(
    "-v",
    "--verbose",
    help="If set, output details of the execution",
    action="store_true",
)
parser.add_argument(
    "--tf-log-level",
    help="Tensorflow minimum cpp log level. Default is 0",
    choices=["0", "1", "2", "3"],
    default="0",
)

# Global parameters
# -------------------------

args = parser.parse_args()
verbose = args.verbose

os.environ["TF_CPP_MIN_LOG_LEVEL"] = args.tf_log_level

src.create_dir_if_not_found(src.MODELS_DIR)
src.create_dir_if_not_found(src.RESULTS_DIR)

# GPU configuration
# -------------------------

if args.gpu is not None:
    if verbose:
        print("GPU configuration...", end="")

    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu)

    if verbose:
        print(" Done!")

# Get data
# -------------------------

if verbose:
    print("Getting data...", end="")

if args.mode == "train" or args.mode == "all":
    if args.no_test_data:
        train_input_data, train_policy, train_value = src.generate_batch_data(
            games_data="games/games_elf_train_all_data.data",
            planes=src.PLANES,
            moves=src.MOVES,
            N=100096,
        )

    else:
        train_input_data, train_policy, train_value = src.generate_batch_data(
            games_data="games/games_elf_train.data",
            planes=src.PLANES,
            moves=src.MOVES,
            N=100096,
        )

if not args.no_test_data or args.mode == "info" or args.mode == "test":
    test_input_data = np.load("{}test_input_data.npy".format(src.DATA_DIR))
    test_policy = np.load("{}test_policy.npy".format(src.DATA_DIR))
    test_value = np.load("{}test_value.npy".format(src.DATA_DIR))

if verbose:
    print(" Done!")

# Build model
# -------------------------

if verbose:
    print("Building model...", end="")

# The 999999 one
# model = build_resnet_network(src.INPUT_SHAPE, src.MOVES, 39, 32, 21)

# Version 4 with dropout, version 5 without
model = src.build_resnet_network(src.INPUT_SHAPE, src.MOVES, 10, 62, 64)

if verbose:
    print(" Done!")

# Loading weights
# -------------------------

if args.weights is not None and os.path.exists(args.weights):
    if verbose:
        print("Loading {}...".format(args.weights), end="")

    model.load_weights(args.weights)

    if verbose:
        print(" Done!")

# Model compilation
# -------------------------

if verbose:
    print("Compiling model...", end="")


def accuracy(y_true, y_pred):
    return K.mean(K.equal(y_true, K.sign(y_pred)), axis=-1)


# First: Adadelta with lr=1, then decrease
# Second: SGD with lr=1e-3

model.compile(
    loss={"value": "mean_squared_error", "policy": "categorical_crossentropy"},
    optimizer=tf.keras.optimizers.SGD(lr=1e-3, momentum=0.5),
    # optimizer=tf.keras.optimizers.Adadelta(learning_rate=1),
    # loss_weights={"value": 1.3, "policy": 0.4}, # Not strictly necessary
    metrics={"policy": "accuracy", "value": accuracy},
)

if verbose:
    print(" Done!")

# Train mode
# -------------------------

if args.mode == "train" or args.mode == "all":
    if verbose:
        print("Training model...")

    csv_logger = CSVLogger(args.output_log, append=True, separator=";")
    checkpoint = ModelCheckpoint(
        args.weights,
        verbose=int(verbose),
        save_freq="epoch",
        # monitor="val_loss",
        # save_best_only=True,
    )

    if args.no_test_data:
        validation_data = None
    else:
        validation_data = (
            test_input_data,
            {"policy": test_policy, "value": test_value},
        )

    if not args.dynamic_batch:
        history = model.fit(
            train_input_data,
            {"policy": train_policy, "value": train_value},
            batch_size=args.batch_size,
            validation_data=validation_data,
            epochs=args.epochs,
            verbose=int(verbose),
            callbacks=[csv_logger, checkpoint],
        )

    elif args.dynamic_batch:
        history = model.fit_generator(
            src.DataSequence(
                train_input_data,
                train_policy,
                train_value,
                args.batch_size,
                model=model,
                data_selection=args.data_selection,
                data_augmentation=args.data_augmentation,
            ),
            validation_data=validation_data,
            epochs=args.epochs,
            verbose=int(verbose),
            callbacks=[csv_logger, checkpoint],
        )

    if verbose:
        print("Saving weights...", end="")

    model.save_weights(args.weights)

    if verbose:
        print(" Done!")

# Info mode
# -------------------------

if args.mode == "info" or args.mode == "all":
    if verbose:
        print("Evaluating the model...", end="")

    metrics = model.evaluate(
        test_input_data,
        {"policy": test_policy, "value": test_value},
        batch_size=args.batch_size,
        verbose=0,
    )

    summary_list = []
    model.summary(print_fn=lambda x: summary_list.append(x))
    model_summary = "\n".join(summary_list)

    if verbose:
        print(" Done!")
        print("Saving info...")

    info = "Model evaluation\n# -------------------------\n\n"

    for index_metric in range(len(model.metrics_names)):
        info += "{}: {}\n".format(
            model.metrics_names[index_metric], metrics[index_metric]
        )

    info += "\nModel summary\n# -------------------------\n\n"
    info += model_summary

    if verbose:
        print(info)

    with open(args.output_info, "w") as f:
        f.write(info)
