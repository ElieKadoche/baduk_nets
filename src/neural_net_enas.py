import tensorflow.keras.layers as layers
import tensorflow.keras.regularizers as regularizers
from tensorflow.keras.models import Model


def build_enas_network(input_shape, moves, blocks, filters, value_dense):
    """Network for the value only.

    Obtained with the ENAS module
    """
    input_tensor = layers.Input(shape=input_shape, name="board")

    # LAYER 0
    value_branch = layers.SeparableConv2D(
        filters=filters,
        kernel_size=(3, 3),
        strides=(1, 1),
        padding="same",
        use_bias=False,
        activation=None,
        kernel_regularizer=regularizers.l2(1e-4),
    )(input_tensor)
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)
    value_branch = layers.Dropout(0.2)(value_branch)
    layer_0 = value_branch

    # LAYER 1
    value_branch = layers.AveragePooling2D(
        pool_size=(3, 3), strides=(1, 1), padding="same",
    )(value_branch)
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)
    layer_1 = value_branch

    # LAYER 2
    value_branch = layers.Conv2D(
        filters=filters,
        kernel_size=(1, 1),
        strides=(1, 1),
        padding="same",
        use_bias=False,
        activation=None,
        kernel_regularizer=regularizers.l2(1e-4),
    )(value_branch)
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)
    value_branch = layers.Dropout(0.2)(value_branch)
    layer_2 = value_branch

    # LAYER 3
    value_branch = layers.Conv2D(
        filters=filters,
        kernel_size=(3, 3),
        strides=(1, 1),
        padding="same",
        use_bias=False,
        activation=None,
        kernel_regularizer=regularizers.l2(1e-4),
    )(value_branch)
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)
    value_branch = layers.Dropout(0.2)(value_branch)
    layer_3 = value_branch

    # LAYER 4
    value_branch = layers.AveragePooling2D(
        pool_size=(3, 3), strides=(1, 1), padding="same",
    )(value_branch)
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)
    layer_4 = value_branch

    # LAYER 5
    value_branch = layers.Conv2D(
        filters=filters,
        kernel_size=(1, 1),
        strides=(1, 1),
        padding="same",
        use_bias=False,
        activation=None,
        kernel_regularizer=regularizers.l2(1e-4),
    )(value_branch)
    value_branch = layers.Add()([value_branch, layer_0, layer_2, layer_3])
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)
    value_branch = layers.Dropout(0.2)(value_branch)
    layer_5 = value_branch

    # LAYER 6
    value_branch = layers.Conv2D(
        filters=filters,
        kernel_size=(3, 3),
        strides=(1, 1),
        padding="same",
        use_bias=False,
        activation=None,
        kernel_regularizer=regularizers.l2(1e-4),
    )(value_branch)
    value_branch = layers.Add()([value_branch, layer_1, layer_2, layer_5])
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)
    value_branch = layers.Dropout(0.2)(value_branch)
    layer_6 = value_branch

    # LAYER 7
    value_branch = layers.Conv2D(
        filters=filters,
        kernel_size=(1, 1),
        strides=(1, 1),
        padding="same",
        use_bias=False,
        activation=None,
        kernel_regularizer=regularizers.l2(1e-4),
    )(value_branch)
    value_branch = layers.Add()([value_branch, layer_2, layer_3])
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)
    value_branch = layers.Dropout(0.2)(value_branch)
    layer_7 = value_branch

    # LAYER 8
    value_branch = layers.AveragePooling2D(
        pool_size=(3, 3), strides=(1, 1), padding="same",
    )(value_branch)
    value_branch = layers.Add()([value_branch, layer_4, layer_5, layer_6])
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)
    layer_8 = value_branch

    # LAYER 9
    value_branch = layers.Conv2D(
        filters=filters,
        kernel_size=(3, 3),
        strides=(1, 1),
        padding="same",
        use_bias=False,
        activation=None,
        kernel_regularizer=regularizers.l2(1e-4),
    )(value_branch)
    value_branch = layers.Add()([value_branch, layer_8])
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)
    value_branch = layers.Dropout(0.2)(value_branch)

    # LAYER 10
    value_branch = layers.MaxPooling2D(
        pool_size=(3, 3), strides=(1, 1), padding="same",
    )(value_branch)
    value_branch = layers.Add()(
        [value_branch, layer_0, layer_4, layer_5, layer_7, layer_8])
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)

    # LAYER 11
    value_branch = layers.Conv2D(
        filters=filters,
        kernel_size=(3, 3),
        strides=(1, 1),
        padding="same",
        use_bias=False,
        activation=None,
        kernel_regularizer=regularizers.l2(1e-4),
    )(value_branch)
    value_branch = layers.Add()([value_branch, layer_1, layer_6, layer_7])
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)
    value_branch = layers.Dropout(0.2)(value_branch)

    value_branch = layers.Flatten()(value_branch)
    value_branch = layers.Dense(
        1, activation="sigmoid", name="value")(value_branch)

    return Model(inputs=input_tensor, outputs=value_branch)
