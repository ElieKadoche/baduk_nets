import os

GAMES_DIR = "games/"
MODELS_DIR = "models/"
RESULTS_DIR = "results/"
DATA_DIR = "{}data/".format(GAMES_DIR)

PLANES = 8
MOVES = 361
INPUT_SHAPE = (19, 19, PLANES)


def create_dir_if_not_found(directory_path):
    """ Create the given directory if it does not already exist

    Args:
        directory_path: the path to the directory
    """
    if not os.path.exists(directory_path):
        os.mkdir(directory_path)
