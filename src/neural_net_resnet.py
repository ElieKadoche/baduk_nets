import tensorflow.keras.layers as layers
import tensorflow.keras.regularizers as regularizers
from tensorflow.keras.models import Model


def build_resnet_network(input_shape, moves, blocks, filters, value_dense):
    """ Function to build a resnet network (this is the Alpha Go Zero network).

    Args:
        input_shape (tuple): input shape of tensors

    Returns:
        alphago_zero_model: the final model
    """
    input_tensor = layers.Input(shape=input_shape, name="board")

    X = layers.Conv2D(
        filters=filters,
        kernel_size=(3, 3),
        strides=(1, 1),
        padding="same",
        activation=None,
        use_bias=False,
        kernel_regularizer=regularizers.l2(1e-4),
    )(input_tensor)
    X = layers.BatchNormalization()(X)
    X = layers.Activation("relu")(X)
    # X = layers.Dropout(0.2)(X)

    # Residual blocks
    for i in range(blocks):
        X_shortcut = X

        X = layers.Conv2D(
            filters=filters,
            kernel_size=(3, 3),
            strides=(1, 1),
            padding="same",
            activation=None,
            use_bias=False,
            kernel_regularizer=regularizers.l2(1e-4),
        )(X)
        X = layers.BatchNormalization()(X)
        X = layers.Activation("relu")(X)

        X = layers.Conv2D(
            filters=filters,
            kernel_size=(3, 3),
            strides=(1, 1),
            padding="same",
            activation=None,
            use_bias=False,
            kernel_regularizer=regularizers.l2(1e-4),
        )(X)
        X = layers.BatchNormalization()(X)

        X = layers.Add()([X, X_shortcut])
        X = layers.Activation("relu")(X)

    # Policy head
    policy_head = layers.Conv2D(
        filters=2,
        kernel_size=(1, 1),
        strides=(1, 1),
        padding="same",
        activation=None,
        use_bias=False,
        kernel_regularizer=regularizers.l2(1e-4),
    )(X)
    policy_head = layers.BatchNormalization()(policy_head)
    policy_head = layers.Activation("relu")(policy_head)
    policy_head = layers.Flatten()(policy_head)
    # policy_head = layers.Dropout(0.2)(policy_head)
    policy_head = layers.Dense(
        moves, activation="softmax", name="policy")(policy_head)

    # Value head
    value_head = layers.Conv2D(
        filters=1,
        kernel_size=(1, 1),
        strides=(1, 1),
        padding="same",
        activation=None,
        use_bias=False,
        kernel_regularizer=regularizers.l2(1e-4),
    )(X)
    value_head = layers.BatchNormalization()(value_head)
    value_head = layers.Activation("relu")(value_head)

    value_head = layers.Flatten()(value_head)
    # value_head = layers.Dropout(0.4)(value_head)
    value_head = layers.Dense(value_dense)(value_head)  # 256 in the paper
    value_head = layers.Dense(1, activation="tanh", name="value")(value_head)

    alphago_zero_model = Model(inputs=input_tensor, outputs=[
                               policy_head, value_head])
    return alphago_zero_model
