import tensorflow.keras.layers as layers
import tensorflow.keras.regularizers as regularizers
from tensorflow.keras.models import Model


def build_x2_network(input_shape, moves, blocks, filters, value_dense):
    input_tensor = layers.Input(shape=input_shape, name="board")

    policy_branch = layers.Conv2D(
        filters=42,
        kernel_size=(3, 3),
        strides=(1, 1),
        padding="same",
        activation=None,
        use_bias=False,
        kernel_regularizer=regularizers.l2(1e-4),
    )(input_tensor)
    policy_branch = layers.BatchNormalization()(policy_branch)
    policy_branch = layers.Activation("relu")(policy_branch)
    policy_branch = layers.Dropout(0.2)(policy_branch)

    for i in range(9):
        policy_branch_shortcut = policy_branch

        policy_branch = layers.Conv2D(
            filters=42,
            kernel_size=(3, 3),
            strides=(1, 1),
            padding="same",
            activation=None,
            use_bias=False,
            kernel_regularizer=regularizers.l2(1e-4),
        )(policy_branch)
        policy_branch = layers.BatchNormalization()(policy_branch)
        policy_branch = layers.Activation("relu")(policy_branch)

        policy_branch = layers.Conv2D(
            filters=42,
            kernel_size=(3, 3),
            strides=(1, 1),
            padding="same",
            activation=None,
            use_bias=False,
            kernel_regularizer=regularizers.l2(1e-4),
        )(policy_branch)
        policy_branch = layers.BatchNormalization()(policy_branch)

        policy_branch = layers.Add()([policy_branch, policy_branch_shortcut])
        policy_branch = layers.Activation("relu")(policy_branch)

    policy_branch = layers.Conv2D(
        filters=2,
        kernel_size=(1, 1),
        strides=(1, 1),
        padding="same",
        activation=None,
        use_bias=False,
        kernel_regularizer=regularizers.l2(1e-4),
    )(policy_branch)
    policy_branch = layers.BatchNormalization()(policy_branch)
    policy_branch = layers.Activation("relu")(policy_branch)
    policy_branch = layers.Flatten()(policy_branch)
    policy_branch = layers.Dropout(0.2)(policy_branch)
    policy_branch = layers.Dense(moves, activation="softmax", name="policy")(
        policy_branch
    )

    ###########################
    # Value branch
    ###########################

    value_branch = layers.Conv2D(
        filters=42,
        kernel_size=(3, 3),
        strides=(1, 1),
        padding="same",
        activation=None,
        use_bias=False,
        kernel_regularizer=regularizers.l2(1e-4),
    )(input_tensor)
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)
    # value_branch = layers.Dropout(0.2)(value_branch)

    for i in range(9):
        value_branch_shortcut = value_branch

        value_branch = layers.Conv2D(
            filters=42,
            kernel_size=(3, 3),
            strides=(1, 1),
            padding="same",
            activation=None,
            use_bias=False,
            kernel_regularizer=regularizers.l2(1e-4),
        )(value_branch)
        value_branch = layers.BatchNormalization()(value_branch)
        value_branch = layers.Activation("relu")(value_branch)

        value_branch = layers.Conv2D(
            filters=42,
            kernel_size=(3, 3),
            strides=(1, 1),
            padding="same",
            activation=None,
            use_bias=False,
            kernel_regularizer=regularizers.l2(1e-4),
        )(value_branch)
        value_branch = layers.BatchNormalization()(value_branch)

        value_branch = layers.Add()([value_branch, value_branch_shortcut])
        value_branch = layers.Activation("relu")(value_branch)

    value_branch = layers.Conv2D(
        filters=1,
        kernel_size=(1, 1),
        strides=(1, 1),
        padding="same",
        activation=None,
        use_bias=False,
        kernel_regularizer=regularizers.l2(1e-4),
    )(value_branch)
    value_branch = layers.BatchNormalization()(value_branch)
    value_branch = layers.Activation("relu")(value_branch)
    value_branch = layers.Flatten()(value_branch)
    # value_branch = layers.Dropout(0.2)(value_branch)
    value_branch = layers.Dense(256)(value_branch)
    value_branch = layers.Dense(
        1, activation="sigmoid", name="value")(value_branch)

    policy_model = Model(inputs=input_tensor, outputs=policy_branch)
    value_model = Model(inputs=input_tensor, outputs=value_branch)
    model = Model(inputs=input_tensor, outputs=[policy_branch, value_branch])

    return policy_model, value_model, model
