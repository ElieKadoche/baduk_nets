import math
import random
from statistics import median

import imgaug.augmenters as iaa
import numpy as np
import tensorflow.keras as keras

import golois
import src


def save_npy(prefix, input_data, policy, value):
    """Function to saved numpy objects to disk.

    Args:
        prefix (str): the prefix of output files
        input_data (np_array): inputs data
        policy (np_array): policies
        values (np_array): values
    """
    src.create_dir_if_not_found(src.DATA_DIR)
    np.save(
        "{}{}_input_data.npy".format(src.DATA_DIR, prefix),
        input_data,
        allow_pickle=False,
    )
    np.save("{}{}_policy.npy".format(src.DATA_DIR, prefix),
            policy, allow_pickle=False)
    np.save("{}{}_value.npy".format(src.DATA_DIR, prefix),
            value, allow_pickle=False)


def generate_batch_data(games_data, planes, moves, N):
    """Function to generate random samples.

    Uses get_batch_data function from CPP golois package.

    Args:
        games_data (str): file from which to load data
        planes (int): the number of features
        moves (int): the number of moves
        N (int): the number of examples

    Returns:
        input_data (np_array): inputs data
        policy (np_array): policies
        values (np_array): values
    """
    # Random data
    # -------------------------

    input_data = np.random.randint(2, size=(N, 19, 19, planes))
    input_data = input_data.astype("float32")

    policy = np.random.randint(moves, size=(N,))
    policy = keras.utils.to_categorical(policy)

    value = np.random.randint(2, size=(N,))
    value = value.astype("float32")

    end = np.random.randint(2, size=(N, 19, 19, 2))
    end = end.astype("float32")

    # Golois get_batch_data function
    # -------------------------

    golois.get_batch_data(games_data, input_data, policy, value, end)

    return input_data, policy, value


def generate_all_data(games_data, planes, moves, N):
    """Function to generate all samples.

    Uses get_all_data function from CPP golois package.
    N MUST corresponds to the total number of samples

    Args:
        games_data (str): file from which to load data
        planes (int): the number of features
        moves (int): the number of moves
        N (int): the number of examples

    Returns:
        input_data (np_array): inputs data
        policy (np_array): policies
        values (np_array): values
    """
    # Random data
    # -------------------------

    input_data = np.random.randint(2, size=(N, 19, 19, planes))
    input_data = input_data.astype("float32")

    policy = np.random.randint(moves, size=(N,))
    policy = keras.utils.to_categorical(policy)

    value = np.random.randint(2, size=(N,))
    value = value.astype("float32")

    end = np.random.randint(2, size=(N, 19, 19, 2))
    end = end.astype("float32")

    # Golois get_batch_data function
    # -------------------------

    golois.get_all_data(games_data, input_data, policy, value, end)

    return input_data, policy, value


class DataSequence(keras.utils.Sequence):
    def __init__(
        self,
        x, p, v,
        batch_size,
        model=None,
        data_selection=False,
        data_augmentation=False,
    ):
        self.x = x
        self.p = p
        self.v = v

        self.epoch = 0
        self.model = model

        self.batch_size = batch_size
        self.data_selection = data_selection
        self.data_augmentation = data_augmentation

    def __len__(self):
        """Denotes the number of batches per epoch."""
        return math.ceil(len(self.x) / self.batch_size)

    def __getitem__(self, idx):
        """ Generates one batch of data."""
        x = np.array(
            self.x[idx * self.batch_size: (idx + 1) * self.batch_size])
        p = np.array(
            self.p[idx * self.batch_size: (idx + 1) * self.batch_size])
        v = np.array(
            self.v[idx * self.batch_size: (idx + 1) * self.batch_size])

        if not self.data_augmentation:
            return x, [p, v]

        batch_x, batch_p, batch_v = [], [], []

        for i in range(p.shape[0]):
            # No data augmentation
            # -----------------

            batch_x.append(x[i])
            batch_p.append(p[i])
            batch_v.append(v[i])

            # Data augmentation
            # -----------------

            # Policy position
            p_pos = np.argmax(p[i])
            p_row = p_pos // 19
            p_col = p_pos % 19

            # We add 1 augmentation, otherwise it worsen the training
            # Augmentation does not change the plane <color_to_play>
            # So we can apply it on all the input at once

            x_to_aug = np.expand_dims(x[i], 0)  # Shallow copy here

            rand = random.random()

            # Vertical flip (up / down)
            if rand < 0.5:
                x_aug = iaa.Flipud(1.0)(images=x_to_aug)  # Deepcopy here
                batch_x.append(x_aug[0])

                # Deal with the policy
                p_aug = np.zeros(361)
                aug_p_row = 18 - p_row
                p_aug[19 * aug_p_row + p_col] = 1
                batch_p.append(p_aug)

            # Horizontal flip (left / right)
            else:
                x_aug = iaa.Fliplr(1.0)(images=x_to_aug)  # Deepcopy here
                batch_x.append(x_aug[0])

                # Deal with the policy
                p_aug = np.zeros(361)
                aug_p_col = 18 - p_col
                p_aug[19 * p_row + aug_p_col] = 1
                batch_p.append(p_aug)

                # Augmentation does not change the value
            batch_v.append(v[i])

        x = np.array(batch_x)
        p = np.array(batch_p)
        v = np.array(batch_v)

        return x, [p, v]  # For complete model
        # return x, p  # For policy model
        # return x, v  # For value model

    def on_epoch_end(self):
        """ Modifies data after after_x epochs
        """
        after_x = 2
        self.epoch += 1
        if self.epoch % after_x == 0:
            del self.x, self.p, self.v

            if not self.data_selection:
                self.x, self.p, self.v = generate_batch_data(
                    games_data="{}games_elf_train_all_data.data".format(
                        src.GAMES_DIR),
                    planes=src.PLANES,
                    moves=src.MOVES,
                    N=100096,
                )

            elif self.data_selection:
                self.x, self.p, self.v = generate_batch_data(
                    games_data="{}games_elf_train_all.data".format(
                        src.GAMES_DIR),
                    planes=src.PLANES,
                    moves=src.MOVES,
                    N=200192,  # Taking x2 data, too big on colab
                    # N=102400,  # Taking x2 data, good on colab
                )

                # Get predictions
                po, vo = self.model.predict(self.x, batch_size=self.batch_size)

                # Computing value loss for each sample
                losses = (self.v - vo.flatten()) ** 2

                # Get median
                median_loss = median(losses)

                m = losses >= median_loss
                self.x = self.x[m]
                self.p = self.p[m]
                self.v = self.v[m]
