import argparse
import os

import golois
import src


def create_games_txt(sgf_dir, games_txt):
    sgfs = []

    for root, subdirs, files in os.walk(sgf_dir):
        for f in files:
            sgfs.append(os.path.join(root, f))

    with open(games_txt, "w") as g:
        g.write("\n".join(sgfs))


def train(args):
    sgf_dir = "{}sgf_elf_train".format(src.GAMES_DIR)
    games_txt = "{}games_elf_train.txt".format(src.GAMES_DIR)
    games_data = "{}games_elf_train.data".format(src.GAMES_DIR)

    src.create_dir_if_not_found(src.DATA_DIR)

    print("Creating {}...".format(games_txt), end="")
    create_games_txt(sgf_dir, games_txt)
    print(" Done!")

    print("Creating {}...".format(games_data))
    golois.create_games_data(games_txt, games_data)

    print(" Done!")


def test(args):
    sgf_dir = "{}sgf_elf_test".format(src.GAMES_DIR)
    games_txt = "{}games_elf_test.txt".format(src.GAMES_DIR)
    games_data = "{}games_elf_test.data".format(src.GAMES_DIR)

    print("Creating {}...".format(games_txt), end="")
    create_games_txt(sgf_dir, games_txt)
    print(" Done!")

    print("Creating {}...".format(games_data))
    nbPositionsSGF = golois.create_games_data(games_txt, games_data)

    print("Generating data...")
    x, p, v = src.generate_all_data(
        games_data=games_data,
        planes=src.PLANES,
        moves=src.MOVES,
        N=nbPositionsSGF,
    )

    print("Saving data...", end="")
    src.save_npy("test", x, p, v)
    print(" Done!")


def all_(args):
    sgf_dir_test = "{}sgf_elf_test".format(src.GAMES_DIR)
    sgf_dir_train = "{}sgf_elf_train".format(src.GAMES_DIR)

    games_txt = "{}games_elf_train_all.txt".format(src.GAMES_DIR)
    games_data = "{}games_elf_train_all_data.data".format(src.GAMES_DIR)

    print("Creating {}...".format(games_txt), end="")
    sgfs = []

    for root, subdirs, files in os.walk(sgf_dir_train):
        for f in files:
            sgfs.append(os.path.join(root, f))

    for root, subdirs, files in os.walk(sgf_dir_test):
        for f in files:
            sgfs.append(os.path.join(root, f))

    with open(games_txt, "w") as g:
        g.write("\n".join(sgfs))

    print(" Done!")

    print("Creating {}...".format(games_data))
    golois.create_games_data(games_txt, games_data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script to create data")
    subparsers = parser.add_subparsers()

    parser_train = subparsers.add_parser(
        "train",
        help="Sub-command to create train data",
    )

    parser_test = subparsers.add_parser(
        "test",
        help="Sub-command to create test data",
    )

    parser_all = subparsers.add_parser(
        "all",
        help="Sub-command to create .data for all data",
    )

    parser_train.set_defaults(func=train)
    parser_test.set_defaults(func=test)
    parser_all.set_defaults(func=all_)

    args = parser.parse_args()
    args.func(args)
